from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from recipes.views import CommentDelete
from upload.views import image_upload
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path("upload-files/", image_upload, name="upload"),
    path("admin/", admin.site.urls),
    path('auth/', include('authentication.urls')),
    path('recipes/', include('recipes.urls')),
    path('comments/<int:pk>/', CommentDelete.as_view(), name='delete-single-comment'),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)