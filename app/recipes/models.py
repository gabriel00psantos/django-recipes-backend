from django.db import models

class Recipe(models.Model):
    recipe_title = models.CharField(max_length=30)
    recipe_content = models.CharField(max_length=1000)
    recipe_thumb_url = models.CharField(max_length=50)
    recipe_create_time = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey('authentication.CustomUser', related_name='recipe_owner', on_delete=models.CASCADE)

    @property
    def total_comments(self):
        return self.comment_recipe.count()


class Comment(models.Model):
    comment_content = models.CharField(max_length=140)
    comment_create_time = models.DateTimeField(auto_now=True)
    recipe = models.ForeignKey('recipes.Recipe', related_name="comment_recipe", on_delete=models.CASCADE)
    owner = models.ForeignKey("authentication.CustomUser", related_name="comment_user", on_delete=models.CASCADE)
    