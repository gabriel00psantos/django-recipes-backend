from recipes.models import Recipe, Comment
from recipes.serializers import RecipeSerializer, CommentSerializer
from django.http import Http404
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from recipes.pagination import PaginationHandlerMixin

class BasicPagination(PageNumberPagination):
    page_size_query_param = 'limit'

class RecipeList(APIView, PaginationHandlerMixin):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    pagination_class = BasicPagination
    serializer_class = RecipeSerializer

    def get(self, request, format=None):
        recipes = Recipe.objects.all()
        page = self.paginate_queryset(recipes)
        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page,
                                                     many=True, 
                                                     context={'request': request}).data)
        else:
            serializer = self.serializer_class(recipes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def post(self, request, format=None):
        serializer = RecipeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class RecipeDetail(APIView):
    
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_object(self, pk):
        try:
            return Recipe.objects.get(id=pk)
        except Recipe.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        recipe = self.get_object(pk)
        serializer = RecipeSerializer(recipe, context={'request': request})
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        recipe = self.get_object(pk)
        recipe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ComentList(APIView, PaginationHandlerMixin):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    pagination_class = BasicPagination
    serializer_class = CommentSerializer

    def get_object(self, pk):
        try:
            return Comment.objects.filter(recipe=pk).all()
        except Comment.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        comments = self.get_object(pk)
        page = self.paginate_queryset(comments)
        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page,
                                                     many=True, 
                                                     context={'request': request}).data)
        else:
            serializer = self.serializer_class(comments, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, pk, format=None):
        serializer = CommentSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CommentDelete(APIView):
    
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_object(self, pk):
        try:
            return Comment.objects.get(id=pk)
        except Comment.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        comment = self.get_object(pk)
        serializer = CommentSerializer(comment, context={'request': request})
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        comment = self.get_object(pk)
        comment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)