from django.contrib import admin
from recipes.models import Recipe, Comment

# Register your models here.

class RecipesAdmin(admin.ModelAdmin):
    model = Recipe

class CommentAdmin(admin.ModelAdmin):
    model = Comment

admin.site.register(Recipe, RecipesAdmin)
admin.site.register(Comment, CommentAdmin)