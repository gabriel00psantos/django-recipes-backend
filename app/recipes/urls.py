from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from recipes import views


urlpatterns = format_suffix_patterns([
	path('', views.RecipeList.as_view(), name='recipe-list'),
    path('<int:pk>/', views.RecipeDetail.as_view(), name="single-recipe"),
    path('<int:pk>/comments/', views.ComentList.as_view(), name="list-comments"),
])