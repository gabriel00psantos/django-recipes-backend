from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from .models import Recipe, Comment
from authentication.models import CustomUser
import pdb

class CommentSerializer(serializers.HyperlinkedModelSerializer):
	comment_content = serializers.CharField(allow_blank=False, trim_whitespace=True)
	recipe_id = serializers.CharField(allow_blank=False, trim_whitespace=True)
	owner_id = serializers.ReadOnlyField(source='owner.id')
	owner_name = serializers.ReadOnlyField(source='owner.username')

	comment = serializers.HyperlinkedIdentityField(view_name='delete-single-comment', format='json')
	
	class Meta:
		model = Comment
		fields = ("id", "comment_content", "comment_create_time", "owner_id",
				  "recipe_id", "comment", "owner_name")
		extra_kwargs = {
						'owner_id': {'read_only': True},
						}

	def create(self, validated_data):
		user_id = self.context['request'].user.id
		recipe_id = self.initial_data['recipe_id']
		user = CustomUser.objects.get(id=user_id)
		recipe = Recipe.objects.get(id=recipe_id)
		return Comment.objects.create(**validated_data, 
									  recipe=recipe, 
									  owner=user)


class RecipeSerializer(serializers.HyperlinkedModelSerializer):
	recipe_title = serializers.CharField(allow_blank=False, trim_whitespace=True)
	recipe_content = serializers.CharField(allow_blank=False, trim_whitespace=True)
	recipe_thumb_url = serializers.CharField(allow_blank=False, trim_whitespace=True)
	owner_id = serializers.ReadOnlyField(source='owner.id')
	owner_name = serializers.ReadOnlyField(source='owner.username')
	total_comments = serializers.ReadOnlyField()

	comments = serializers.HyperlinkedIdentityField(view_name='list-comments', format='json')

	class Meta:
		model = Recipe
		fields = ("id", "recipe_title", "recipe_content", "recipe_thumb_url", 
				  "recipe_create_time", "owner_id","owner_name", "total_comments", "comments" )
		
	def validate(self, data):
		if not data:
		    raise serializers.ValidationError("Please fill in the data")
		return data

	def create(self, validated_data):
		user_id = self.context['request'].user.id
		user = CustomUser.objects.get(id=user_id)
		return Recipe.objects.create(**validated_data, owner=user)

	def update(self, instance, validated_data):
	    instance.recipe_title = validated_data.get('recipe_title', instance.recipe_title)
	    instance.recipe_content = validated_data.get('recipe_content', instance.recipe_content)
	    instance.recipe_thumb_url = validated_data.get('recipe_thumb_url', instance.recipe_thumb_url)
	    instance.save()
	    return instance
