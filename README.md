# Django-recipes-backend

Essa aplicação realiza consulta de "Receitas", "Comentarios" e "Usuarios"

Libs Utilizadas

- Django & DjangoRest
- Docker
- PostgresSQL


## Construa o ambiente

    docker-compose up --b --d

## Rode as migrations

	docker-compose exec web python manage.py migrate

## Crie o super usuario do Django (user: gabriel, senha: pass123), ou se cadastre na aplicação front-end https://bitbucket.org/gabriel00psantos/django-recipes-front-end/
	
	docker-compose exec web python manage.py createsuperuser --noinput 

# REST API

	Se tudo deu certo, a aplicação estara disponivel no endereço http://localhost:8000/


![alt text](https://i.kym-cdn.com/entries/icons/mobile/000/028/021/work.jpg)